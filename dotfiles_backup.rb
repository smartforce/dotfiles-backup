#!/usr/bin/env ruby
#

require 'rubygems'
# Used for Configuration
# https://ruby-doc.org/stdlib-2.4.1/libdoc/yaml/rdoc/YAML.html
require 'yaml'
# http://stackoverflow.com/a/12617369
require 'fileutils'
# https://github.com/rubyzip/rubyzip
require 'zip'
# https://github.com/piotrmurach/tty-prompt
require 'tty-prompt'
# Colors (https://github.com/piotrmurach/pastel#3-supported-colors)
require 'pastel'
# Date http://stackoverflow.com/a/12544638
require 'date'

class DotfilesBackup
  # Get YAML Config
  @@confFile = File.join(File.dirname(__FILE__), 'dotfiles_backup.yml')
  @@yaml_conf = YAML.load_file(@@confFile)
  @@srcRootFolder = File.expand_path(@@yaml_conf['config']['backup']['src']['root'])
  @@backupFolder = @@yaml_conf['config']['backup']['dst']['location']
  @@backupFileName = @@yaml_conf['config']['backup']['dst']['zip_file_name']

  # Init Pastel
  @@pastel = Pastel.new(eachline: "\n")

  # Init
  def initialize
    sort_conf_files
  end

  # Main program
  def goDotfilesBackup
    begin

      # Get date
      t = Time.now
      t.to_s
      date_backup = t.strftime "%d-%m-%Y_%H%M%S"

      # Init TTY-Prompt
      prompt = TTY::Prompt.new(interrupt: :signal)

      # Create TTY-Prompt
      result = prompt.select('Select task: ', help: '', active_color: :yellow) do |menu|
        menu.default 1
        menu.choice 'Run Backup', 1
        menu.choice 'Add File', 2
        menu.choice 'Copy to Folder', 3
        menu.choice 'Save to Zip', 4
        menu.choice 'Setup Backup Location', 5
        menu.choice 'Setup or overwrite Git repository', 6
      end

      case result

        # Copy to Folder
        when 1
          @@yaml_conf['config']['files'].each do |filename|
            file = File.expand_path(@@srcRootFolder + '/' + filename)
            copy_with_path(file)
          end
          # Save to Zip
          backupFileNameZip = File.expand_path(@@srcRootFolder + '/' + @@backupFolder + '/' + @@backupFileName + '_' + date_backup + '.zip')
          # backupFileNameZip = @@backupFolder + '/' + @@backupFileName + '_' + date_backup + '.zip'
          create_zip(backupFileNameZip, date_backup)

        # Add File
        when 2
          newDotFile = prompt.ask("File relative to ~/")
          case newDotFile
          when *@@yaml_conf['config']['files']
            puts "File " + @@pastel.red.bold("#{@@srcRootFolder}/#{newDotFile}") + " already exists!"
          else
            if File.exists? File.expand_path(@@srcRootFolder + '/' + newDotFile)
              @@yaml_conf['config']['files'].push(newDotFile)
              File.open(@@confFile, 'w') {|f| f.write @@yaml_conf.to_yaml } #Store
              sort_conf_files
            else
              puts "File " + @@pastel.red.bold("#{@@srcRootFolder}/#{newDotFile}") + " does not exist!"
            end
          end


        # Copy to Folder
        when 3
          @@yaml_conf['config']['files'].each do |filename|
            file = File.expand_path(@@srcRootFolder + '/' + filename)
            copy_with_path(file)
          end

        # Save to Zip
        when 4
          backupFileNameZip = File.expand_path(@@srcRootFolder + '/' + @@backupFolder + '/' + @@backupFileName + '_' + date_backup + '.zip')
          # backupFileNameZip = @@backupFolder + '/' + @@backupFileName + '_' + date_backup + '.zip'
          create_zip(backupFileNameZip, date_backup)

        # Setup Backup Location
        when 5
          oldBackupLocation ||= @@yaml_conf['config']['backup']['dst']['location']
          newBackupLocation = prompt.ask("Backup Location relative to ~/", default: oldBackupLocation)
          puts "Location set to: #{newBackupLocation}" if check_folder(newBackupLocation)

        # Setup or overwrite Git repository
        when 6
          # @variable ||= "set value if not set"
          oldGitRepoURL ||= @@yaml_conf['config']['git']['repository']
          newGitRepoURL = prompt.ask("Git Repository URL", default: oldGitRepoURL)
          @@yaml_conf['config']['git']['repository'] = newGitRepoURL
          File.open(@@confFile, 'w') {|f| f.write @@yaml_conf.to_yaml } #Store

      end

    # Intercept key interruption
    rescue Interrupt
      puts
      puts
      puts @@pastel.bold('Exiting...')
    end
  end


  # Check if backup folder exists and create a new folder
  def check_folder(path)
    path = File.expand_path(path)
    unless File.exists? path
        FileUtils.mkdir_p(path)
    end
  end

  # Check if backup folder exists and create a new folder
  def check_file(path)
    path = File.expand_path(path)
    unless File.exists? path
        return true
    end
  end

  # copy (and overwrite destination)
  def copy_with_path(src)
    check_folder(@@srcRootFolder + '/' + @@backupFolder)
    if File.exists? src
      FileUtils.cp_r(src, @@srcRootFolder + '/' + @@backupFolder, :remove_destination => true)
      puts "Copied " + @@pastel.green("#{src}") + ' to Backup'
    end
  end

  # remove backup dir
  def remove_backup_dir(dir)
    if File.exists? dir
      FileUtils.rmdir(dir)
    end
  end

  # Create Zip
  def create_zip(backupFileNameZip, date_backup)
    Zip::File.open(backupFileNameZip, Zip::File::CREATE) do |zipfile|
      @@yaml_conf['config']['files'].each do |filename|
        file = File.expand_path(@@srcRootFolder + '/' + filename)

        if File.exists? file
          # Two arguments:
          # - The name of the file as it will appear in the archive
          # - The original file, including the path to find it
          zipfile.add(filename, file)
          # puts "Adding #{@@srcRootFolder}/#{filename} to Zipfile #{}"
        else
          puts "File " + @@pastel.red.bold("#{@@srcRootFolder}/#{filename}") + " does not exist!"
        end
      end
    end
    puts "Saved to " + @@pastel.green.bold("#{backupFileNameZip}")
  end

  # sort hash
  def sort_conf_files
    hash = @@yaml_conf['config']['files']
    #sorted = sort_hash(hash.to_yaml)
    yaml = hash.sort
    @@yaml_conf['config']['files'] = yaml
    File.open(@@confFile, 'w') {|f| f.write @@yaml_conf.to_yaml } #Store
  end

end

# finally, run the damn thing!
DotfilesBackup.new.goDotfilesBackup
